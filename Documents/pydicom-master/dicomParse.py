import pydicom as dicom
import os
import numpy
from matplotlib import pyplot
import pylab

dir = "/Users/lousanna/Desktop/XTK/087/"
files = []

for f in os.listdir(dir):
	if f.endswith('.dcm'):
		files.append(os.path.join(dir,f))

ds = dicom.read_file(files[0])
rows = int(ds.Rows)
cols = int(ds.Columns)
dimShape = (rows, cols, len(files))
spacing = (float(ds.PixelSpacing[0]), float(ds.PixelSpacing[1]), float(ds.SliceThickness))

x = numpy.arange(0.0, (dimShape[0]+1)*spacing[0], spacing[0])
y = numpy.arange(0.0, (dimShape[1]+1)*spacing[1], spacing[1])
threeDArray = numpy.empty(dimShape, dtype=ds.pixel_array.dtype)

for file in files:
	curr = dicom.read_file(file)
	threeDArray[:,:,files.index(file)] = curr.pixel_array

pylab.figure(dpi=100)

for n,val in enumerate(ds.pixel_array.flat):
     if val < -500:
         ds.pixel_array.flat[n]=0
ds.PixelData = ds.pixel_array.tostring()
ds.save_as("contrast.dcm")

change = numpy.rollaxis(threeDArray, 2,0)
A = change.shape
ds.pixel_array.flat = change[100,:,:]
ds.PixelData = ds.pixel_array.tostring()
numpy.resize(ds.pixel_array,(A[0],A[1]))
dicom.write_file("test1.dcm",ds)

pylab.imshow(ds.pixel_array, cmap=pylab.cm.bone)
pylab.show()
