//
//TicTacToeGame.cpp
//
//Created by Lousanna Cai on 4/13/16

#include "game.h"
#include "tictactoe.h"
using namespace std;

//Makes a move on the grid.
GameResult TicTacToeGame::attack_square(Coord coordinate){

    int x = coordinate.first;
    int y = coordinate.second;
    if (x>2 || y>2 || x<0 || y<0 || ticBoard.get_value(coordinate)!=0 || get_game_status()){
        return RESULT_INVALID;
    }
    add_counter();
    int player_num = return_player();
    ticBoard.set_grid(make_pair(x,y),player_num);
    print(); //comment out if output unwanted
    return check_status();
}

//Sets initial state of grid.
void TicTacToeGame::setTTT(){
    for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
        ticBoard.set_grid(make_pair(i,j),0);
        }
    }
}

//Checks the current status of the game.
GameResult TicTacToeGame::check_status(void){

    int stat = ticBoard.get_value(make_pair(0,0)); //check diagonal
    if(stat!=0
       && stat==ticBoard.get_value(make_pair(1,1))
       && ticBoard.get_value(make_pair(1,1))==ticBoard.get_value(make_pair(2,2))) {
        return get_win(stat);
    }
    
    stat = ticBoard.get_value(make_pair(0,2)); //check diagonal
    if(stat!=0
       && stat==ticBoard.get_value(make_pair(1,1))
       && ticBoard.get_value(make_pair(1,1))==ticBoard.get_value(make_pair(2,0))) {
        return get_win(stat);
    }
    
    for (int i=0;i<3;i++){ //check horizontals and verticals
        stat=ticBoard.get_value(make_pair(0,i));
        if ( stat!= 0 && stat==ticBoard.get_value(make_pair(1,i))
            && ticBoard.get_value(make_pair(1,i))==ticBoard.get_value(make_pair(2,i))) {
            return get_win(stat);
        }
        stat = ticBoard.get_value(make_pair(i,0));
        if ( stat!=0 && stat==ticBoard.get_value(make_pair(i,1))
            && ticBoard.get_value(make_pair(i,1))==ticBoard.get_value(make_pair(i,2))) {
            return get_win(stat);
        }
    }
    
    if (get_Counter() >= 9){
        set_game_status();
        return RESULT_STALEMATE;
    }

    return RESULT_KEEP_PLAYING;
}

//Returns which person won.
GameResult TicTacToeGame::get_win(int stat){
    
    set_game_status();

    if (stat==1)
    return RESULT_PLAYER1_WINS;
    if (stat==2)
    return RESULT_PLAYER2_WINS;
    
    return RESULT_KEEP_PLAYING; //won't be returned because get_win is only called after win
}

//Prints the TTT board. 0 is player 1, X is player 2.
void TicTacToeGame::print(){
    
    char symbol[] = {'-','0','X'};
    cout << "\n";
    for (int i=0;i<3;i++){
        for(int j=0;j<3; j++){
            int curr = ticBoard.get_value(make_pair(i,j));
            cout << symbol[curr] << " ";
        }
        cout << "\n";
    }
}

