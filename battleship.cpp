//
//  battleship.cpp
//  hw5
//
//  Created by HuangXinyu on 4/12/16.
//

#include "battleship.h"

using namespace std;

//attack a square
//return RESULT_INVALID if the coordinate given is out of boundary
//or it has been attacked before
//return RESULT_PLAYER1_WINS if player 1 wins
//return RESULT_PLAYER2_WINS if player 2 wins
//return RESULT_KEEP_PLAYING if the game has not ended
GameResult BattleshipGame::attack_square(Coord coordinate){
    int x = coordinate.first;
    int y = coordinate.second;
    //check if the game has ended
    if (get_game_status())
	return  RESULT_INVALID;
    //check if the coordinates given is out of boundary
    if (x>9 || y>9 || x<0 || y<0){
        return RESULT_INVALID;
    }
    add_counter();
    int player_num = return_player();
    //if the grid has been attacked before
    if (battleship[player_num-1].get_value(make_pair(x,y)) == -1){
	minus_counter();
        return RESULT_INVALID;
    }
    battleship[player_num-1].set_grid(make_pair(x,y),-1);
    return check_status();
}

//initialize the board with the given array
void BattleshipGame::setBattleship(int shipNum, array<Coord,5> player){
    for (int i=0;i<5;i++){
	if (battleship[shipNum].get_value(player[i])==1)
		init_status = false;
	else 
        battleship[shipNum].set_grid(player[i],1);
    }
}

//check whether a game ends or not
GameResult BattleshipGame::check_status(void){
    bool flag1 = false;//false means player 1 wins
    bool flag2 = false;//false means player 2 wins
    for (int i=0;i<10;i++){
        for (int j=0;j<10;j++){
            if (battleship[0].get_value(make_pair(i,j)) == 1)
                flag1 = true;
            if (battleship[1].get_value(make_pair(i,j)) == 1)
                flag2 = true;
        }
    }
    if (!flag1){
        set_game_status();
        return RESULT_PLAYER1_WINS;
    }
    if (!flag2){
	set_game_status();
        return RESULT_PLAYER2_WINS;
    }
    return RESULT_KEEP_PLAYING;
}

bool BattleshipGame::get_initialization_status()
{
	return init_status;
}
