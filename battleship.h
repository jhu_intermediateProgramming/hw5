//
//  battleship.h
//  hw5
//
//  Created by HuangXinyu on 4/12/16.
//

#include "game.h"
#include <array>

class BattleshipGame:public Game{
public:
    BattleshipGame(std::array<Coord,5> player1, std::array<Coord,5> player2){
        setBattleship(0, player1);
        setBattleship(1, player2);
    }
    GameResult attack_square(Coord coordinate);
    bool get_initialization_status();
private:
    Board battleship[2] = {Board(10),Board(10)};

    //initialize the Board
    void setBattleship(int shipNum,std::array<Coord,5> player);

    //check whether the game has ended or not
    GameResult check_status(void);
    bool init_status = true;
};
