#include "battleship.h"
#include <iostream>

using namespace std;

void game1()
{
	array<Coord, 5> player1_ships = {{
		make_pair(2, 2),
		make_pair(4, 4),
		make_pair(5, 5),
		make_pair(6, 6),
		make_pair(8, 8)
	}};
	array<Coord, 5> player2_ships = {{
		make_pair(2, 2),
		make_pair(4, 4),
		make_pair(5, 5),
		make_pair(6, 6),
		make_pair(8, 8)
	}};
	BattleshipGame bsg(player1_ships, player2_ships);

	GameResult result = bsg.attack_square(make_pair(2, 2)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(2, 2)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	// oops, already an X on this square
	result = bsg.attack_square(make_pair(2, 2)); // P1 invalid move
	assert(result == RESULT_INVALID);
	
	// note: an invalid move doesn't count as a turn; still player 1's turn
	
	result = bsg.attack_square(make_pair(4, 4)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(4, 4)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 5)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(5, 5)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 6)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(6, 6)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(8, 8)); // P1 HIT
	assert(result == RESULT_PLAYER1_WINS);

	cout << "PASSED game 1" << endl;
}

void game2(){
	array<Coord, 5> player1_ships = {{
		make_pair(9, 5),
		make_pair(9, 6),
		make_pair(9, 7),
		make_pair(9, 8),
		make_pair(9, 9)
	}};
	array<Coord, 5> player2_ships = {{
		make_pair(9, 5),
		make_pair(9, 6),
		make_pair(9, 7),
		make_pair(9, 8),
		make_pair(9, 9)
	}};

	BattleshipGame bsg(player1_ships, player2_ships);

	GameResult result;
	for (int i= 0; i < 9 ; i++){

		for (int j = 0 ; j < 9 ; j++){
			result = bsg.attack_square(make_pair(i, j)); // P1 Turn
			assert(result == RESULT_KEEP_PLAYING);
			result = bsg.attack_square(make_pair(i, j)); // P2 Turn
			assert(result == RESULT_KEEP_PLAYING);
			result = bsg.attack_square(make_pair(i, j)); // P1 Turn Invalid
			assert(result == RESULT_INVALID);
			result = bsg.attack_square(make_pair(i, j)); // P2 Turn Invalid
			assert(result == RESULT_INVALID);

		}


	}
	
	result = bsg.attack_square(make_pair(9,15555)); // P1 Turn
        assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(155555,9)); // P1 Turn
        assert(result == RESULT_INVALID);

	
	result = bsg.attack_square(make_pair(9, 0)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 0)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 1)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 1)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 2)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 2)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 3)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 3)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 4)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 4)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);


	result = bsg.attack_square(make_pair(9, 5)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 5)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);


	result = bsg.attack_square(make_pair(9, 5)); // P1 Turn Ivalid
	assert(result == RESULT_INVALID);
	result = bsg.attack_square(make_pair(9, 6)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 6)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 7)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 7)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 8)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 8)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 9)); // P1 Wins
	assert(result == RESULT_PLAYER1_WINS);

	result = bsg.attack_square(make_pair(9, 9)); // P2 is an idiot and is trying to attack after the game is over
        assert(result == RESULT_INVALID);


	cout << "PASSED game 2" << endl;
}


//exact same as game 2, but player 1 messes up so player 2 wins first. Latest possible turn for P2 to win.
void game3(){

	array<Coord, 5> player1_ships = {{
		make_pair(9, 5),
		make_pair(9, 6),
		make_pair(9, 7),
		make_pair(9, 8),
		make_pair(9, 9)
	}};
	array<Coord, 5> player2_ships = {{
		make_pair(9, 5),
		make_pair(9, 6),
		make_pair(9, 7),
		make_pair(9, 8),
		make_pair(9, 9)
	}};

	BattleshipGame bsg(player1_ships, player2_ships);

	GameResult result;
	for (int i= 0; i < 9 ; i++){

		for (int j = 0 ; j < 9 ; j++){
			result = bsg.attack_square(make_pair(i, j)); // P1 Turn
			assert(result == RESULT_KEEP_PLAYING);
			result = bsg.attack_square(make_pair(i, j)); // P2 Turn
			assert(result == RESULT_KEEP_PLAYING);

		}


	}

	result = bsg.attack_square(make_pair(9, 0)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 0)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 1)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 1)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 2)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 2)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 3)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 3)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 4)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 5)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);


	result = bsg.attack_square(make_pair(9, 5)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 6)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 6)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 7)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 7)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 8)); // P2 Turn
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 8)); // P1 Turn
	assert(result == RESULT_KEEP_PLAYING);
	result = bsg.attack_square(make_pair(9, 9)); // P2 Wins. 
	assert(result == RESULT_PLAYER2_WINS);


	cout << "PASSED game 3" << endl;
}

//invalid initialization, putting two ships on one square when initializing

void game4(){

	array<Coord, 5> player1_ships = {{
		make_pair(9, 5),
		make_pair(9, 6),
		make_pair(9, 7),
		make_pair(9, 7),
		make_pair(9, 9)
	}};
	array<Coord, 5> player2_ships = {{
		make_pair(9, 5),
		make_pair(9, 5),
		make_pair(9, 7),
		make_pair(9, 8),
		make_pair(9, 9)
	}};

	BattleshipGame bsg(player1_ships, player2_ships);

	assert(bsg.get_initialization_status() ==false );
	cout << "PASSED game 4" << endl;
}

int main(void)
{
	game1();
	game2();
	game3();
	game4();
	return 0;
}
