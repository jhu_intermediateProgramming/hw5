//
//  tictactoe.h
//  hw5
//
//  Created by Lousanna Cai on 4/13/16.
//
//

#include "game.h"

class TicTacToeGame:public Game
{
    public:
    TicTacToeGame(){
    setTTT();
    };
    GameResult attack_square(Coord coordinate);
    void print();

    private:
    Board ticBoard = Board(3);
    void setTTT();
    GameResult check_status(void);
    GameResult get_win(int stat);
};

